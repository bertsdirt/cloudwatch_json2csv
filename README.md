# cloudwatch_json2csv 

> I've been collecting output from Amazon AWS Cloudwatch via the aws cli, but it is in JSON format and I have a need for it is CSV (for Excel).  This little script will convert the Cloudwatch JSON into a CSV file for me.

#Requirements
 Python

 (cloudwatch_cpuutil.json sample included in repo)

# Install
 ???

# Execute:
 python ./cloudwatch_json2csv.py -i cloudwatch_cpuutil.json -o cloudwatch_cpuutil.csv
